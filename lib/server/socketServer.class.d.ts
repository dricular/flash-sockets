import * as WebSocket from 'ws';
export declare const createSocketServer: (options?: WebSocket.ServerOptions) => Promise<{}>;
