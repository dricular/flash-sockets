"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const WebSocket = require("ws");
class SocketServer {
    constructor(socketServer) {
        this.socketServer = socketServer;
        this.handleConnection = (socket, req) => {
            console.log('New Socket connection: ', req.url);
            socket.on('message', (message) => this._forwardMessageFrom(socket, message));
        };
        socketServer.on('connection', this.handleConnection);
    }
    _forwardMessageFrom(socket, message) {
        for (let client of this.socketServer.clients) {
            if (client !== socket) {
                client.send(message);
            }
        }
    }
    close() {
        return new Promise((resolve, reject) => {
            this.socketServer.close((error) => {
                if ('undefined' === typeof error) {
                    resolve();
                }
                else {
                    reject(error);
                }
            });
        });
    }
}
exports.createSocketServer = (options) => {
    const wsserver = new WebSocket.Server(options);
    return new Promise((resolve, reject) => {
        let server;
        wsserver.on('listening', () => resolve(server));
        wsserver.on('error', reject);
        server = new SocketServer(wsserver);
    });
};
//# sourceMappingURL=socketServer.class.js.map