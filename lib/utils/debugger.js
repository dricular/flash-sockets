"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.debugEvents = (eventNames, label) => (target, ...args) => {
    const logLabel = `\x1b[1m${label || 'Event'}\x1b[0m`;
    const logEvent = (eventName) => (...eventArgs) => {
        console.log(`${logLabel} \x1b[1m%s\x1b[0m %s`, ...args, eventName, eventArgs);
    };
    for (let index = 0; index < eventNames.length; index++) {
        const eventName = eventNames[index];
        target.on(eventName, logEvent(eventName));
    }
};
//# sourceMappingURL=debugger.js.map