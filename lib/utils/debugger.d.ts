/// <reference types="node" />
import { EventEmitter } from 'events';
export declare const debugEvents: <E extends string>(eventNames: E[], label?: string) => (target: EventEmitter, ...args: any[]) => void;
