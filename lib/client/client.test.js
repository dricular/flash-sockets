"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("./client");
const socketServer_class_1 = require("../server/socketServer.class");
const serverConfig = {
    port: 3001,
    host: '127.0.0.1',
    path: '/',
};
describe(`testing node client`, function () {
    let client;
    let server;
    const onClientConnect = jest.fn();
    beforeAll(() => __awaiter(this, void 0, void 0, function* () {
        server = yield socketServer_class_1.createSocketServer(serverConfig);
        client = client_1.createSocketClient(serverConfig);
        client.on('open', onClientConnect);
    }));
    afterAll(() => __awaiter(this, void 0, void 0, function* () {
        yield client.close();
        yield server.close();
    }));
    it('onClientConnect called once', () => {
        expect(onClientConnect).toBeCalledTimes(1);
    });
}); // testing node client
//# sourceMappingURL=client.test.js.map