import * as WebSocket from 'ws';
export { WebSocket };
export declare namespace createClient {
    interface Options extends WebSocket.ClientOptions {
        host?: string;
        pathname?: string;
        port?: number;
    }
}
export declare const buildSocketURL: (options?: createClient.Options) => string;
export declare const createSocketClient: (options?: createClient.Options) => WebSocket;
