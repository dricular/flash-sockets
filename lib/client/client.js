"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const WebSocket = require("ws");
exports.WebSocket = WebSocket;
const path_1 = require("path");
const debugger_1 = require("../utils/debugger");
exports.buildSocketURL = (options = {}) => {
    const { host = '127.0.0.1', port = 80 } = options;
    const hostname = port !== 80 ? `${host}:${port}` : host;
    const pathname = path_1.join('/', options.pathname || '').slice(1);
    return `ws://${hostname}/${pathname}`;
};
exports.createSocketClient = (options = {}) => {
    const { host = '127.0.0.1', protocol = 'echo', pathname = '/' } = options, wsOptions = __rest(options
    /** omit redundant config */
    , ["host", "protocol", "pathname"]);
    /** omit redundant config */
    const socketOptions = Object.assign({}, wsOptions, { protocol: undefined, host: undefined, port: undefined });
    delete socketOptions.protocol;
    delete socketOptions.host;
    delete socketOptions.port;
    const url = exports.buildSocketURL(options);
    if (process.env.NODE_ENV === 'debug') {
        console.log('connect client to %s', url, socketOptions);
    }
    const socket = new WebSocket(url, protocol, socketOptions);
    if (process.env.NODE_ENV === 'debug') {
        debugger_1.debugEvents(['open', 'close', 'message', 'error'], 'ClientEvent')(socket);
    }
    return socket;
};
//# sourceMappingURL=client.js.map