# Flash sockets

Socket relais server for simple chat messaging among clients using [ws](https://github.com/websockets/ws)


## Usage

- Start server
```shell
./bin/startServer.js
```

- Start client
```shell
./bin/startClient.js
```

After connecting, the client will read `stdin` linewise and send input to the server, which will forward it to all other clients.