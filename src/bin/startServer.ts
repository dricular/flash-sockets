#!/usr/bin/env node

import { createSocketServer } from '../lib/server/socketServer.class'

const host = '127.0.0.1'
const port = 3002

createSocketServer({
  port,
  host,
}).then(server => {
  console.log('Server started ws://%s:%d/', host, port )
})
