#!/usr/bin/env node

import { createSocketClient } from '../lib/client/client'
import { Data } from 'ws'

const client = createSocketClient({
  port: 3002,
  host: '127.0.0.1'
})
client.on('message', (data: Data) => {
  console.log('Message: %s', data)
})
process.stdin.on('data', (data: Buffer | string) => {
  client.send(data)
})
process.stdin.resume()


client.on('error',(error: Error) => {
  console.error(error)
  process.exit(1)
})

client.on('close', (code: number) => {
  //process.stdin.pause()
  console.log('client closed with code %d', code)
  process.exit(code)
})
