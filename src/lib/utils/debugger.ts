import * as http from 'http'
import { EventEmitter } from 'events'

export const debugEvents = <E extends string>(eventNames: E[], label?: string) => (
  target: EventEmitter,
  ...args: any[]
) => {
  const logLabel = `\x1b[1m${label || 'Event'}\x1b[0m`
  const logEvent = (eventName: string) => (...eventArgs: any[]) => {
    console.log(`${logLabel} \x1b[1m%s\x1b[0m %s`, ...args, eventName, eventArgs)
  }

  for (let index = 0; index < eventNames.length; index++) {
    const eventName = eventNames[index]
    target.on(eventName, logEvent(eventName))
  }
}
