import * as WebSocket from 'ws'
import { IncomingMessage } from 'http'

class SocketServer {
  constructor(readonly socketServer: WebSocket.Server) {
    socketServer.on('connection', this.handleConnection)
  }

  handleConnection = (socket: WebSocket, req: IncomingMessage) => {
    console.log('New Socket connection: ', req.url)
    socket.on('message', (message: WebSocket.Data) => this._forwardMessageFrom(socket, message))
  }

  private _forwardMessageFrom(socket: WebSocket, message: WebSocket.Data) {
    for (let client of this.socketServer.clients) {
      if (client !== socket) {
        client.send(message)
      }
    }
  }

  close() {
    return new Promise((resolve, reject) => {
      this.socketServer.close((error?:Error) => {
        if ( 'undefined' === typeof error ) {
          resolve()
        } else {
          reject(error)
        }
      })
    })
  }
}

export const createSocketServer = (options?: WebSocket.ServerOptions) => {
  const wsserver = new WebSocket.Server(options)
  return new Promise((resolve, reject) => {
    let server: SocketServer
    wsserver.on('listening', () => resolve(server))
    wsserver.on('error', reject)
    server = new SocketServer(wsserver)    
  })
}
