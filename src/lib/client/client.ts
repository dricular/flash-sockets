import * as WebSocket from 'ws'
import { join } from 'path'
import { debugEvents } from '../utils/debugger'

export { WebSocket }

export namespace createClient {
  export interface Options extends WebSocket.ClientOptions {
    host?: string
    pathname?: string
    port?: number
  }
}

export const buildSocketURL = ( options:createClient.Options = {} ) => {
  const { host = '127.0.0.1', port = 80 } = options

  const hostname = port !== 80 ? `${host}:${port}` : host
  const pathname = join('/', options.pathname || '').slice(1)
  return `ws://${hostname}/${pathname}`
}

export const createSocketClient = (options: createClient.Options = {}) => {
  const { host = '127.0.0.1', protocol = 'echo', pathname = '/', ...wsOptions } = options

  /** omit redundant config */
  const socketOptions = {
    ...wsOptions,
    protocol: undefined,
    host: undefined,
    port: undefined,
  }
  delete socketOptions.protocol
  delete socketOptions.host
  delete socketOptions.port
  
  const url = buildSocketURL(options)

  if (process.env.NODE_ENV === 'debug') {
    console.log('connect client to %s', url, socketOptions)
  }
  const socket = new WebSocket(url, protocol, socketOptions)
  if (process.env.NODE_ENV === 'debug') {
    debugEvents(['open', 'close', 'message', 'error'], 'ClientEvent')(socket)
  }
  return socket
}
