import { createSocketClient, WebSocket } from './client'
import { createSocketServer } from '../server/socketServer.class'

const serverConfig = {
  port: 3001,
  host: '127.0.0.1',
  path: '/',
}

describe(`testing node client`, function() {
  let client: WebSocket
  let server

  const onClientConnect = jest.fn()

  beforeAll(async() => {
    server = await createSocketServer(serverConfig)
    client = createSocketClient(serverConfig)
    client.on('open', onClientConnect)
  })

  afterAll(async () => {
    await client.close()
    await server.close()
  })

  it('onClientConnect called once', () => {
    expect(onClientConnect).toBeCalledTimes(1)
  })
}) // testing node client
