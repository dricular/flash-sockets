#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("../lib/client/client");
const client = client_1.createSocketClient({
    port: 3002,
    host: '127.0.0.1'
});
client.on('message', (data) => {
    console.log('Message: %s', data);
});
process.stdin.on('data', (data) => {
    client.send(data);
});
process.stdin.resume();
client.on('error', (error) => {
    console.error(error);
    process.exit(1);
});
client.on('close', (code) => {
    //process.stdin.pause()
    console.log('client closed with code %d', code);
    process.exit(code);
});
//# sourceMappingURL=startClient.js.map