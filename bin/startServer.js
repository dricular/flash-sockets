#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const socketServer_class_1 = require("../lib/server/socketServer.class");
const host = '127.0.0.1';
const port = 3002;
socketServer_class_1.createSocketServer({
    port,
    host,
}).then(server => {
    console.log('Server started ws://%s:%d/', host, port);
});
//# sourceMappingURL=startServer.js.map