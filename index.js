"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./lib/server/socketServer.class"));
__export(require("./lib/client/client"));
//# sourceMappingURL=index.js.map